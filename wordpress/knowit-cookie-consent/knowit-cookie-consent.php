<?php
/**
 * @package Knowit-Cookie-Consent
 */
/*
Plugin Name: Cookie Consent by Knowit
Plugin URI: https://github.com/fadichmn/knowit-cookie-consent
Description: A very simple plugin Cookie Consent plugin that is free to use, made by Fadi Chamoun / Knowit
Version: 1.0
Author: Fadi Chamoun
Author URI: mailto:fadi.chamoun@knowit.se
License: GPLv2 or later
*/
if(!class_exists("KnowitCookieConsent")) {
    class KnowitCookieConsent {
        function __construct() {
            global $pagenow;

            require_once(sprintf("%s/includes/settings.php", dirname(__FILE__)));
            add_action('acf/init', function(){
                $settings = new KnowitCookieConsent_Settings(plugin_basename(__FILE__));
                $settings->Knowit_ACF_Fields();
            }, 10);
            if (isset($pagenow) && (($pagenow == 'admin.php' && $pagenow != 'upload.php') || !is_admin())) {
                // Cookie consent script and fields
                require_once(sprintf("%s/includes/consent.php", dirname(__FILE__)));

                // Cookie consent css styling
                require_once(sprintf("%s/includes/assets/styling.php", dirname(__FILE__)));
            }

        }
    }

    $plugin = new KnowitCookieConsent();
}