<?php
/*
* @package Knowit-Cookie-Consent
*/

function knowit_cookie_consent_content(){ 

// Cookie consent js script included
require_once(sprintf("%s/assets/script.php", dirname(__FILE__)));

$neka_cookies_knapp = get_field('neka_cookies_knapp', 'option');
$hantering_av_cookies = get_field('hantering_av_cookies', 'option');

if($hantering_av_cookies != true){?>
    <style type="text/css">
    .cc-nb-changep { 
        display: block !important;
        visibility: hidden !important;
    }
    </style>
<?php } ?>
<!-- Cookie Consent Start -->
<script type="text/javascript" charset="UTF-8">
document.addEventListener('DOMContentLoaded', function () {
    cookieconsent.run({"notice_banner_type":"simple","consent_type":"express","palette":"light","language":"sv","page_load_consent_levels":["strictly-necessary"],"notice_banner_reject_button_hide":<?php if($neka_cookies_knapp == true){ echo "false"; } else { echo "true"; }; ?>,"preferences_center_close_button_hide":false,"open_preferences_center_selector":false,"website_privacy_policy_url":""});
});
</script>

<?php 
    // Nödvändiga cookies
    $nodvandiga_script = get_field('nodvandiga_script', 'option');
    if(!empty($nodvandiga_script)){ ?>
        <?php echo preg_replace('/(<script\b[^><]*)>/i', '$1 cookie-consent="strictly-necessary" async  >', $nodvandiga_script); ?>
    <?php } 
?>

<?php 
    // Personaliseringscookies
    $personalisering_script = get_field('personalisering_script', 'option');
    if(!empty($personalisering_script)){ ?>
        <?php echo preg_replace('/(<script\b[^><]*)>/i', '$1 cookie-consent="functionality" async  >', $personalisering_script); ?>
    <?php } 
?>

<?php 
    // Optimeringscookies
    $optimering_script = get_field('optimering_script', 'option');
    if(!empty($optimering_script)){ ?>
        <?php echo preg_replace('/(<script\b[^><]*)>/i', '$1 cookie-consent="tracking" async  >', $optimering_script); ?>
    <?php } 
?>
<!-- Cookie Consent End -->
<?php }
add_action('wp_head', 'knowit_cookie_consent_content');
