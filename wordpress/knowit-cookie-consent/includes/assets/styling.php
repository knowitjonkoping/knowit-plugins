<?php
/*
* @package Knowit-Cookie-Consent
*/
add_action( 'wp_head', 'knowit_cookie_consent_custom_css');

function knowit_cookie_consent_custom_css() {

    if(get_field('acceptera_cookies_knapp_bg', 'option')){
        $acceptera_cookies_knapp_bg = get_field('acceptera_cookies_knapp_bg', 'option');
    } else {
        $acceptera_cookies_knapp_bg = "#333333";
    }
    if(get_field('acceptera_cookies_knapp_textfarg', 'option')){
        $acceptera_cookies_knapp_textfarg = get_field('acceptera_cookies_knapp_textfarg', 'option');
    } else {
        $acceptera_cookies_knapp_textfarg = "#ffffff";
    }
    
    ?>
    <style>
    .knowitCookieConsent---nb-simple {
        min-width: 100% !important;
        box-shadow: 0px 0px 10px 1px rgba(0, 0, 0, 0.15);
    }
    .knowitCookieConsent---nb .cc-nb-main-container {
      padding: 2rem !important;
      max-width: 1044px;
      margin: 0 auto;
    }
    @media screen and (max-width: 769px){
      .knowitCookieConsent---nb .cc-nb-main-container {
          padding: 2rem !important;
      }
      .cc-pc-head-title-headline img {
          display: none !important;
      }
      .knowitCookieConsent---pc-dialog .cc-pc-head-title {
          padding: 0 !important;
      }
    }
    .cc-cp-foot,
    .cc-pc-head {
        padding: 10px 0;
    }
    .knowitCookieConsent---pc-dialog .cc-pc-head-lang {
        min-height: auto !important;
    }
    .cc-pc-head-lang-select,
    .cc-pc-head-title-text {
        display: none !important;
        visibility: hidden !important;
    }
    .cc-nb-reject {
        background: #7c7c7c !important;
        color: #fff !important;
    }
      
    .knowitCookieConsent---palette-light .cc-nb-okagree {
        background: <?=$acceptera_cookies_knapp_bg;?> !important;
        color: <?=$acceptera_cookies_knapp_textfarg;?> !important;
    }
    .cc-pc-head-title-headline img {
      width: 150px;
      position: absolute;
      right: 0;
      top: 0;
      padding: 10px;
    }
  
    .wpadminbar-active {
      margin-top: 32px;
    }
    </style>
<?php }