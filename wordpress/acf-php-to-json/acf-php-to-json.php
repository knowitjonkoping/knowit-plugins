<?php
/**
 * Plugin Name: ACF PHP to JSON
 * Author: Fadi Chamoun
 * Description: Automatically convert ACF fields from a PHP file to JSON and display the result to copy or download.
 * Version: 1.0
 */

namespace AcfPhpToJson;


function init() {
    add_action('admin_menu', __NAMESPACE__ . '\\add_plugin_submenu_page');
	add_action('admin_enqueue_scripts', __NAMESPACE__ . '\\enqueue_admin_scripts');
}
add_action('plugins_loaded', __NAMESPACE__ . '\\init');

function enqueue_admin_scripts() {
    wp_enqueue_style('prism', plugin_dir_url(__FILE__) . 'includes/css/prism.css');
    wp_enqueue_script('prism', plugin_dir_url(__FILE__) . 'includes/js/prism.js', [], '1.29.0', true);
}

function add_plugin_submenu_page() {
    add_submenu_page(
        'edit.php?post_type=acf-field-group',
        __('Php to Json', 'acf-php-to-json'),
        __('Php to Json', 'acf-php-to-json'),
        'manage_options',
        'acf-php-to-json',
        __NAMESPACE__ . '\\plugin_submenu_page'
    );
}

function plugin_submenu_page() {
    $groups = get_groups_to_convert();

    if (empty($groups)) {
        echo '<div class="wrap">';
        echo '<h1>' . __('Php to Json', 'acf-php-to-json') . '</h1>';
        echo '<p>' . __('No PHP field group configuration found. Nothing to convert.', 'acf-php-to-json') . '</p>';
        echo '</div>';
        return;
    }

    $json = [];

    foreach ($groups as $group) {
        $fields = acf_get_fields($group['key']);

        unset($group['ID']);

        $group['fields'] = $fields;

        $json[] = $group;
    }

    $json_data = json_encode($json, JSON_PRETTY_PRINT);

    ?>
    <div class="wrap">
        <h1><?php echo __('ACF PHP to JSON', 'acf-php-to-json'); ?></h1>
		<pre style="height: 400px; width: 1000px; max-width: 100%;"><?php echo esc_html($json_data); ?></pre>
		<button id="copy-to-clipboard" class="button button-primary"><?php echo __('Copy to Clipboard', 'acf-php-to-json'); ?></button>
		<a href="<?php echo 'data:text/json;charset=utf-8,' . rawurlencode($json_data); ?>" download="acf-import.json" class="button"><?php echo __('Download JSON', 'acf-php-to-json'); ?></a>
	</div>
	<script>
    document.addEventListener('DOMContentLoaded', function(event) {
        const codeElement = document.querySelector('pre');
        codeElement.classList.add('language-javascript');
        Prism.highlightElement(codeElement);
        const copyButton = document.getElementById('copy-to-clipboard');
        copyButton.addEventListener('click', function(event) {
            copyToClipboard(codeElement.textContent);
        });
    });
    function copyToClipboard(text) {
        const textArea = document.createElement('textarea');
        textArea.value = text;
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand('copy');
        document.body.removeChild(textArea);
    }
</script>

<style>
    pre {
        margin: 0;
        padding: 0;
        border: solid 1px #d1d1d1;
    }
    pre.language-javascript {
        display: block;
        overflow-x: auto;
        padding: 0.5em;
        background-color: #111;
        color: #fff;
        border: solid 1px #333;
		margin-bottom: 1em;
		border-radius: 3px;
    }
</style>

<?php }
function get_groups_to_convert() {
	$groups = acf_get_local_field_groups();

	if (!$groups) {
		return [];
	}

	return array_filter($groups, function($group) {
		return $group['local'] == 'php';
	});
}
