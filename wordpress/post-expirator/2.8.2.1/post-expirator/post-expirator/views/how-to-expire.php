<?php
defined('ABSPATH') or die('Direct access not allowed.');

if (! isset($opts['name'])) {
    return false;
}
if (! isset($opts['id'])) {
    $opts['id'] = $opts['name'];
}
if (! isset($opts['type'])) {
    $opts['type'] = '';
}

// Maybe settings have not been configured.
if (empty($opts['type']) && isset($opts['post_type'])) {
    $opts['type'] = $opts['post_type'];
}

?>
<select name="<?php echo esc_attr($opts['name']); ?>" id="<?php echo esc_attr($opts['id']); ?>" class="pe-howtoexpire">
    <option value="avpublicerad" <?php selected($opts['selected'], 'avpublicerad', true); ?>>
        <?php esc_html_e('Avpublicerad', 'post-expirator'); ?>
    </option>
</select>
